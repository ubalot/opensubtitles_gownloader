package cliargs

import (
	"errors"
	"flag"
	"os"
	"path"
	"strings"

	"github.com/adrg/xdg"
)

type CliArgs struct {
	AnonMode bool
	EnvPath  string
	FilePath string
	DirPath  string
	Language string
	Config   bool
	Version  bool
}

var ErrWrongArgumentsUsage = errors.New("wrong cli arguments usage")

func getProgramName() string {
	progName := strings.Split(os.Args[0], string(os.PathSeparator))
	return progName[len(progName)-1]
}

func EnvDefaultPath() string {
	return path.Join(xdg.ConfigHome, getProgramName(), "env.json")
}

func (args CliArgs) customRuleCheck() (err error) {
	if flag.NFlag() == 0 {
		println("at least one argument must be provided\n")
		return ErrWrongArgumentsUsage
	}

	if !args.AnonMode && len(args.Language) > 0 {
		println("language flag must be used with anon mode\n")
		return ErrWrongArgumentsUsage
	}

	if !args.Config && !args.Version && (len(args.FilePath) == 0 && len(args.DirPath) == 0) {
		println("file or directory flag must be provided\n")
		return ErrWrongArgumentsUsage
	}

	if len(args.FilePath) > 0 {
		_, err = os.Stat(args.FilePath)
		if err != nil {
			if os.IsNotExist(err) {
				return errors.New("File " + args.FilePath + " does not exist.\n")
			}
			return
		}
	}

	if len(args.DirPath) > 0 {
		_, err = os.Stat(args.DirPath)
		if err != nil {
			if os.IsNotExist(err) {
				return errors.New("Directory " + args.DirPath + " does not exist.\n")
			}
			return
		}
	}

	return
}

func HandleCliArgs() (args CliArgs, err error) {
	flag.BoolVar(&args.AnonMode, "a", false, "anon mode for OpenSubtitles login")
	flag.BoolVar(&args.AnonMode, "anon", false, "anon mode for OpenSubtitles login")

	flag.BoolVar(&args.Config, "c", false, "print config file path and exit")

	envDefaultPath := EnvDefaultPath()
	flag.StringVar(&args.EnvPath, "e", envDefaultPath, "env.json file path")
	flag.StringVar(&args.EnvPath, "env", envDefaultPath, "env.json file path")

	flag.StringVar(&args.FilePath, "f", "", "file path")
	flag.StringVar(&args.FilePath, "file", "", "file path")

	flag.StringVar(&args.DirPath, "d", "", "directory path")
	flag.StringVar(&args.DirPath, "dir", "", "directory path")

	flag.StringVar(&args.Language, "l", "", "language (to be used with anon mode)")
	flag.StringVar(&args.Language, "lang", "", "language (to be used with anon mode)")

	flag.BoolVar(&args.Version, "v", false, "show version and exit")

	flag.Parse()

	err = args.customRuleCheck()

	return
}
