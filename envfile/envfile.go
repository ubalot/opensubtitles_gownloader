package envfile

import (
	"encoding/json"
	"io"
	"log"
	"os"
	"path/filepath"
)

type Env struct {
	// Opensubtitles_API_key string   `json:"opensubtitles_api_key"`
	Username  string   `json:"username"`
	Password  string   `json:"password"`
	Languages []string `json:"languages"`
}

func createFile(envPath string) (err error) {
	configDir := filepath.Dir(envPath)
	_, err = os.Stat(configDir)
	if os.IsNotExist(err) {
		err = os.MkdirAll(configDir, 0755)
		if err != nil {
			return
		}
	}
	data := Env{Username: "", Password: "", Languages: []string{"eng"}}
	file, _ := json.MarshalIndent(data, "", " ")
	err = os.WriteFile(envPath, file, 0644)
	return
}

func (env *Env) readFile(envPath string) (err error) {
	_, err = os.Stat(envPath)
	if err != nil {
		return
	}

	jsonFile, err := os.Open(envPath)
	if err != nil {
		return
	}

	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	byteValue, err := io.ReadAll(jsonFile)
	if err != nil {
		return
	}

	err = json.Unmarshal([]byte(byteValue), env)
	if err != nil {
		return
	}

	return
}

func (env *Env) Init(anonMode bool, language string, envPath string) (err error) {
	if anonMode {
		if len(language) > 0 {
			env = &Env{Username: "", Password: "", Languages: []string{language}}
		} else {
			env = &Env{Username: "", Password: "", Languages: []string{"eng"}}
		}
	} else {
		// if user configuration file doesn't exist, create it
		_, err = os.Stat(envPath)
		if err != nil {
			if os.IsNotExist(err) {
				err = createFile(envPath)
				if err != nil {
					return
				}
				log.Println("created configuration file: " + envPath)
			}
		}

		// read user configurations
		err = env.readFile(envPath)
		if err != nil {
			return
		}

		if len(env.Languages) == 0 {
			log.Println("`languages` in file `" + envPath + "` is an empty array")
		}
	}
	return
}
