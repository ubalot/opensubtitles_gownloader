package osclient

import (
	"log"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/oz/osdb"
	"gitlab.com/ubalot/opensubtitles_gownloader/envfile"
)

type OpenSubtitlesAPI struct {
	client *osdb.Client
}

func (osAPI *OpenSubtitlesAPI) Init(env envfile.Env) (err error) {
	// Open a new OpenSubtitles client
	osAPI.client, err = osdb.NewClient()
	if err != nil {
		log.Println(err)
		return
	}

	if len(env.Username) > 0 {
		// Try user authentication
		log.Println("Try OpenSubtites credentials login attempt...")
		err = osAPI.client.LogIn(env.Username, env.Password, env.Languages[0])
		if err != nil {
			log.Println(err)
		} else {
			log.Println("OpenSubtites credentials login succeded")
			// client.Token is now set, and subsequent API calls will not be refused.
			return
		}
	}

	if len(env.Languages) > 0 {
		// Try anonimous login with user config language
		log.Println("Try OpenSubtites anonymous login attempt ...")
		err = osAPI.client.LogIn("", "", env.Languages[0])
		if err != nil {
			log.Println(err)
		} else {
			// client.Token is now set, and subsequent API calls will not be refused.
			log.Println("OpenSubtites anonymous login succeded")
			return
		}
	}

	// Try anonimous login with default language
	log.Println("Try OpenSubtites anonymous login with default language (eng) ...")
	err = osAPI.client.LogIn("", "", "eng")
	if err != nil {
		log.Println(err)
	} else {
		// client.Token is now set, and subsequent API calls will not be refused.
		log.Println("OpenSubtites anonymous login with default language (eng) succeded")
		return
	}

	return
}

func isVideoFile(filePath string) bool {
	switch filepath.Ext(filePath) {
	case ".3g2", ".3gp", ".amv", ".asf", ".avi", ".drc", ".mkv", ".mov", ".qt", ".mp4", ".m4p", ".m4v", ".mp2", ".mpe", ".mpv", ".mpg", ".mpeg", ".m2v":
		return true
	default:
		return false
	}
}

func (osAPI *OpenSubtitlesAPI) SearchFile(filePath string, languages []string) error {
	// Get hash movie file, and search...
	subs, err := osAPI.client.FileSearch(filePath, languages)
	if err != nil {
		return err
	}

	if len(subs) == 0 {
		log.Println("[info] no subtitle found for file " + filePath)
	} else {
		// Write subtitles to files
		dirPath := filepath.Dir(filePath)
		filename := filepath.Base(filePath)
		for _, sub := range subs {
			subtitleFilename := filename[:len(filename)-len(filepath.Ext(filename))] + "." + sub.ISO639 + "." + sub.SubFormat
			subtitlePath := path.Join(dirPath, subtitleFilename)
			err := osAPI.client.DownloadTo(&sub, subtitlePath)
			if err != nil {
				return err
			}
			log.Println("[Downloaded] " + subtitlePath)
		}
	}

	return nil
}

func (osAPI *OpenSubtitlesAPI) SearchFiles(dirPath string, languages []string) error {
	log.Println("[Exploring directory] " + dirPath)
	err := filepath.Walk(dirPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if isVideoFile(path) {
			err = osAPI.SearchFile(path, languages)
			if err != nil {
				log.Println("Subtitle for path `" + path + "` can't be downloaded: " + err.Error())
				// return err
			}
			time.Sleep(time.Millisecond * 500) // wait some time before doing another query to OpenSubtitles
		}
		return nil
	})
	return err
}
