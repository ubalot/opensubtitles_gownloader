module gitlab.com/ubalot/opensubtitles_gownloader

go 1.20

require (
	github.com/adrg/xdg v0.4.0
	github.com/oz/osdb v0.0.0-20221214175751-f169057712ec
)

require (
	github.com/kolo/xmlrpc v0.0.0-20220921171641-a4b6fa1dd06b // indirect
	golang.org/x/sys v0.15.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
