package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/debug"

	"gitlab.com/ubalot/opensubtitles_gownloader/cliargs"
	"gitlab.com/ubalot/opensubtitles_gownloader/envfile"
	"gitlab.com/ubalot/opensubtitles_gownloader/osclient"
)

const (
	CliArgsError = iota
	EnvFileError
	OpenSubsCredentialsError
	OpenSubsClientError
)

func getCliArgs() (args cliargs.CliArgs) {
	args, err := cliargs.HandleCliArgs()
	if err != nil {
		switch err {
		case cliargs.ErrWrongArgumentsUsage:
			flag.Usage()
		default:
			fmt.Println(err)
		}
		os.Exit(CliArgsError)
	}
	return args
}

func printProgramVersion() {
	version := "unknown"
	if info, ok := debug.ReadBuildInfo(); ok {
		version = info.Main.Version
	}
	fmt.Println("version:", version)
}

func printProgramConfigFilePath() {
	fmt.Println(cliargs.EnvDefaultPath())
}

func getOpenSubtitlesCredentials(args cliargs.CliArgs) (env envfile.Env) {
	err := env.Init(args.AnonMode, args.Language, args.EnvPath)
	if err != nil {
		fmt.Println(err)
		flag.Usage()
		os.Exit(EnvFileError)
	}
	return env
}

func getOpenSubtitlesClient(env envfile.Env) (client osclient.OpenSubtitlesAPI) {
	err := client.Init(env)
	if err != nil {
		log.Println("Unable to initialize a client for OpenSubtitles:", err.Error())
		os.Exit(OpenSubsCredentialsError)
	}
	return
}

func main() {
	args := getCliArgs()

	if args.Version {
		printProgramVersion()
		return
	}

	if args.Config {
		printProgramConfigFilePath()
		return
	}

	env := getOpenSubtitlesCredentials(args)

	if args.AnonMode {
		log.Println("[*] --- Anon mode ---")
	} else {
		log.Println("[*] --- Env mode ---")
	}

	// Retrieve OpenSubtitles client
	client := getOpenSubtitlesClient(env)

	// File argument
	if len(args.FilePath) > 0 {
		err := client.SearchFile(args.FilePath, env.Languages)
		if err != nil {
			log.Println("Error while looking for subtitles for file:", args.FilePath)
			fmt.Println(err)
			os.Exit(OpenSubsClientError)
		}
	}

	// Directory argument
	if len(args.DirPath) > 0 {
		err := client.SearchFiles(args.DirPath, env.Languages)
		if err != nil {
			log.Println("Error while looking for subtitles in directory:", args.DirPath)
			fmt.Println(err)
			os.Exit(OpenSubsClientError)
		}
	}
}
