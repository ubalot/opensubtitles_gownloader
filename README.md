# Opensubtitles Gownloader

A cli tool that downloads subtitles from [opensubtitles.com](https://www.opensubtitles.com).

In order to use it create a `env.json` in $XDG_CONFIG directory (you can use `env_example.json` as a reference).\
You can leave the fields `username` and `password` empty if you prefer an anonymous login to OpenSubtitles API.\
You can even choose a different location for the `env.json` file. If so, use the `-e` argument and specify the path for the json file.

---

## Debug
### Run
```bash
go run .
```

---

## Build

For local architecture (x86-64)
```bash
go build
```

For arm
```bash
env GOOS=linux GOARCH=arm GOARM=5 go build
```
